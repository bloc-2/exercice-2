package fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.media;

import java.io.BufferedReader;
import java.io.IOException;

/**
 * A java-doc-er
 */
public interface ResumableImageFrameMedia extends ImageFrameMedia {

    /**
     * méthode permettant de reprendre la lecture du fichier
     * @param resume
     * @return
     * @throws IOException
     */
    BufferedReader getEncodedImageReader(boolean resume) throws IOException;
}
