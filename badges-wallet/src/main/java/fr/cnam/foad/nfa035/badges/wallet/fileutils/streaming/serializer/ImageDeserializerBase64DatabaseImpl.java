package fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.serializer;

import fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.media.AbstractImageFrameMedia;
import fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.media.ResumableImageFrameMedia;
import org.apache.commons.codec.binary.Base64InputStream;

import java.io.*;

public class ImageDeserializerBase64DatabaseImpl implements DatabaseDeserializer<ResumableImageFrameMedia> {

    private OutputStream sourceOutputStream;

    /**
     * constructeur
     * @param sourceOutputStream
     */
    public ImageDeserializerBase64DatabaseImpl(OutputStream sourceOutputStream) {
        this.sourceOutputStream = sourceOutputStream;
    }

    /**
     * désérialise media ligne par ligne
     * @param media
     * @throws IOException
     */
    @Override
    public void deserialize(ResumableImageFrameMedia media) throws IOException {
        BufferedReader br = media.getEncodedImageReader(true);
        String[] data = br.readLine().split(";");
        try (OutputStream os = getSourceOutputStream()) {
            getDeserializingStream(data[2]).transferTo(os);
        }
    }

    /**
     * retourne la variable de l'obje instancié
     * @return
     */
    @Override
    public  OutputStream getSourceOutputStream() {return sourceOutputStream;}

    /**
     * écrit ok dans la variable sourcroutpustream de l'objet instancié
     * @param os
     * @param <T>
     */

    @Override
    public <T extends OutputStream> void setSourceOutputStream(T os) {
                this.sourceOutputStream = os;
    }

    /**
     * creée un Inpustream encodé en base 64 à partir d'un string en passant par un bytearrauinpustream
     * @param data
     * @return
     * @throws IOException
     */
    @Override
    public InputStream getDeserializingStream(String data) throws IOException {
        return new Base64InputStream(new ByteArrayInputStream(data.getBytes()));
    }
}