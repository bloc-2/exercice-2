package fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.media.impl;

import fr.cnam.foad.nfa035.badges.wallet.dao.BadgeWalletDAO;
import fr.cnam.foad.nfa035.badges.wallet.fileutils.simpleaccess.ImageSerializer;
import fr.cnam.foad.nfa035.badges.wallet.fileutils.simpleaccess.ImageSerializerBase64Impl;
import fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.media.impl.MultiBadgeWalletDAOImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


import java.io.*;
import java.nio.file.Files;
import java.util.Arrays;




public class SingleBadgeWalletDAOimpl implements BadgeWalletDAO {

    private static final Logger LOG = LogManager.getLogger(SingleBadgeWalletDAOimpl.class);

    private static final String RESOURCES_PATH = "src/test/resources/";
    private static final File walletDatabase = new File(RESOURCES_PATH+ "wallet.csv");

    /**
     * Constructeur élémentaire
     * @param s
     */
    public SingleBadgeWalletDAOimpl(String s) {

    }



}
