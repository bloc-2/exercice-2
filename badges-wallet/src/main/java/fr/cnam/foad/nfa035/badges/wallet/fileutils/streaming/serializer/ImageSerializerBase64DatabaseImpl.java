package fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.serializer;

import fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.media.ImageFrameMedia;
import org.apache.commons.codec.binary.Base64OutputStream;

import java.io.*;
import java.nio.file.Files;

public class ImageSerializerBase64DatabaseImpl
        extends AbstractStreamingImageSerializer<File, ImageFrameMedia> {
    /**
     * getter
      * @param source
     * @return
     * @throws FileNotFoundException
     */
    public InputStream getSourceInputStream(File source) throws FileNotFoundException { return new FileInputStream(source); }

    /**
     * retourne un outpustream en base 64 à partir d'un imageframe media
      * @param media
     * @return
     * @throws IOException
     */
    @Override
    public OutputStream getSerializingStream(ImageFrameMedia media) throws IOException {
        return new Base64OutputStream(media.getEncodedImageOutput());
    }

    /**
     * serialise le File source et écrit la ligne suivante sur media avec les infos ID taille et serialisation de l'image
      * @param source
     * @param media
     * @throws IOException
     */
    @Override
    public final void serialize(File source, ImageFrameMedia media) throws IOException {
        long numberOfLines = Files.lines(((File)media.getChannel()).toPath()).count();
        long size = Files.size(source.toPath());
        try(OutputStream os = getSerializingStream(media)) {
            Writer writer = new OutputStreamWriter(os);
            writer.write(numberOfLines + ";" + size + ";" + os);
            writer.flush();
            try(OutputStream eos = getSerializingStream(media)) {
                getSourceInputStream(source).transferTo(eos);
                writer.write("\n");
            }
            writer.flush();
        }
    }

}