package fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming;

import fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.media.ImageFrameMedia;

import java.io.IOException;
import java.io.OutputStream;

/**
 * donne la possibilité d'utiliser 3 methodes utiles pour la désérialisation de badges
 * @param <M>
 */
public interface BadgeDeserializer<M extends ImageFrameMedia> {
    /**
     * donne une méthode pour désérialiser un média
      * @param media
     * @throws IOException
     */
    void deserialize(M media) throws IOException;

    /**
     * donne un getter pour la source oputstream
      * @return
     * @param <T>
     */
    <T extends OutputStream> T getSourceOutputStream();

    /**
     * donne un setter pour la source oputstream
     * @param os
     * @param <T>
     */
    <T extends OutputStream> void setSourceOutputStream(T os);
}